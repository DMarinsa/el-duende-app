import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Kinvey } from 'kinvey-nativescript-sdk';
import { Usuario } from "~/dataModels/usuario";

/* ***********************************************************
* Before you can navigate to this page from your app, you need to reference this page's module in the
* global app router module. Add the following object to the global array of routes:
* { path: "perfil", loadChildren: "./perfil/perfil.module#PerfilModule" }
* Note that this simply points the path to the page module file. If you move the page, you need to update the route too.
*************************************************************/

@Component({
    selector: "Perfil",
    moduleId: module.id,
    templateUrl: "./perfil.component.html"
})
export class PerfilComponent implements OnInit {
    usuarioActivo: any;
    constructor( private routerExtensions: RouterExtensions) {
        this.usuarioActivo = Kinvey.User.getActiveUser();
    }

    ngOnInit(): void {
        
    }

    goBack(){
        this.routerExtensions.backToPreviousPage();
    }
}
